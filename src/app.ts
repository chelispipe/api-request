import express from 'express';
// libreria para request backend
const axios = require('axios');
var bodyParser = require('body-parser');

const app = express();
const port = 3000;

app.use(bodyParser.urlencoded({ extended: false }))
.use(bodyParser.json({ limit: '200mb' }));

app.get('/get', (req: any, res: any)=> {
  const url = 'http://localhost:4000/api/v1/get/data';
  const headers = {
    'Authorization': `Bearer token_is_here`
  }
  axios.get(url,  {headers})
  .then((resp: any) => {
    res.status(200).send(resp['data']);
  })
  .catch((err: any) =>{
    console.log('err', err);
    res.status(400).send({error: {
      ok: false,
      message: 'No fue posible consultar los datos'
    }});
  });
})

app.post('/registrar_cliente', (req: any, res: any) => {
  const data = req.body;
    let result: any = {};
    const url = 'http://localhost:4000/api/v1/auth/app/login/';
    const payload = {
        apiKey: 'test-api',
        apiSecret: 'Admin*5'
      };
    
    let token = ''
    // petición post al api para obtener un token
    axios.post(url, payload)
      .then( (response: any) =>{
        /* la respuesta del otro server esta dentro de un objeto data
        según sea la respuesta response['data']. se debe agregar despues
        del punto (.)
        */
        token = response['data'].data.token
        const headers = {
          'Authorization': `Bearer ${token}`
        }
        // petición para guardar un cliente
        const urlCustomer = 'http://localhost:4000/api/v1/app/customers/new/';
        // la configuración de la petición se envia al final y entre llaves {}
        axios.post(urlCustomer, data, {headers})
        .then((resp: any) => {
          res.status(200).send(resp['data']);
        })
        .catch((err: any) =>{
          console.log('err', err);
          res.status(400).send({error: {
            ok: false,
            message: 'No fue posible guardar el cliente'
          }});
          });
        })
      .catch( (error: any) =>{
        result = error;
        res.status(200).send(result);
      });
  
});

app.put('/put', (req: any, res: any)=> {
  const data = req.body;
  const url = 'http://localhost:4000/api/v1/put/users/1';
  const headers = {
    'Authorization': `Bearer token_is_here`
  }
  axios.put(url, data, {headers})
  .then((resp: any) => {
    res.status(200).send(resp['data']);
  })
  .catch((err: any) =>{
    console.log('err', err);
    res.status(400).send({error: {
      ok: false,
      message: 'No fue posible actualizar los datos'
    }});
  });
})

app.delete('/delete/:id', (req: any, res: any)=> {
  const id = req.params.id;
  const url = `http://localhost:4000/api/v1/delete/users/${id}`;
  const headers = {
    'Authorization': `Bearer token_is_here`
  }
  axios.delete(url, {headers})
  .then((resp: any) => {
    res.status(200).send(resp['data']);
  })
  .catch((err: any) =>{
    console.log('err', err);
    res.status(400).send({error: {
      ok: false,
      message: 'No fue posible eliminar el usuario'
    }});
  });
})


app.listen(port, () => {
  return console.log(`server is listening on ${port}`);
});