# Api Petición Server - Server

Servidor nodejs typescript - ejemplo, para conectarse haciendo peticiones http entre servidores, utilizando la libreria axios.

# Tener en cuenta
- las url de los endpoint o de las peticiones al otro server.
- los puertos de ejecición no pueden ser iguales entre ambos servers.
- el server tiene get, post, put y delete, cambiar lógica según la necesidad.

# Reconstruir Módulos
```
npm install
```

# Ejecutar el proyecto
```
npm run start
```

# Hacer uso del API
- PORT: Puerto de ejecución
- ROUTE: Ruta del endpoint o servicio
```
http://localhost:PORT/ROUTE
```

# Contacto

- Desarrollador: Felipe Medel
- Email: luispipemedel@gmail.com
